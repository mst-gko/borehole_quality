/* author: andam, simak <simak@mst.dk> maintainance: simak <simak@mst.dk> */ 

BEGIN;

	DROP TABLE IF EXISTS mstjupiter.borehole_quality CASCADE;
	
	CREATE TABLE mstjupiter.borehole_quality AS 
		(
			SELECT  
				b.boreholeno, 
				b.boreholeid,
				tmps.drilldepth_edit,
				CASE 
					WHEN upper(b.use) = 'S'
						THEN 't'::boolean
					ELSE 'f'::boolean
					END AS is_demolished,
				b.geom
			FROM jupiter.borehole b
			LEFT JOIN 
				(
					SELECT b2.boreholeno, b2.boreholeid, COALESCE(max(b2.drilldepth), max(ls2.bottom)) AS drilldepth_edit
					FROM jupiter.borehole b2
					LEFT JOIN jupiter.lithsamp ls2 ON b2.boreholeid = ls2.boreholeid
					GROUP BY b2.boreholeno, b2.boreholeid
				) AS tmps ON b.boreholeid = tmps.boreholeid 
		)
	;

	-- Add primary key
	ALTER TABLE mstjupiter.borehole_quality 
		ADD PRIMARY KEY (boreholeid);

	-- Create geometry index
	CREATE INDEX borehole_quality_geom_idx
  		ON mstjupiter.borehole_quality
			USING GIST (geom);

    -- Add columns
    ALTER TABLE mstjupiter.borehole_quality
    	ADD COLUMN quality_position INTEGER DEFAULT 0,
    	ADD COLUMN quality_litho INTEGER DEFAULT 0
   	;
   
    -- Qry_coastline_1 -- Is borehole within coastline
    --ALTER TABLE mstjupiter.borehole_quality 
    --ADD COLUMN onshore BOOLEAN DEFAULT NULL;
    --UPDATE mstjupiter.borehole_quality AS b
    --SET onshore = st_within(b.geom, kom.geom)
    --FROM grukos_fdw.kommunegraense kom
   	--WHERE b.geom IS NOT NULL;
    
	-- Qry_position_1_Lokaliseringsskema
    UPDATE mstjupiter.borehole_quality AS b
    SET quality_position = 1
    WHERE b.boreholeid IN 
		(
			SELECT DISTINCT bd.boreholeid
			FROM jupiter.boredoc bd
			WHERE upper(bd.doctype) = 'L' 
				--AND (sd.comments NOT ILIKE '%skan%' AND sd.comments NOT ILIKE '%scan%')
		)
	;

    -- Qry_position_2 -- coordinates exist
    UPDATE mstjupiter.borehole_quality AS b
    SET quality_position = 2
    WHERE boreholeid IN 
		(
			SELECT DISTINCT b2.boreholeid
			FROM jupiter.borehole b2
			WHERE (b2.xutm32euref89 IS NOT NULL AND b2.yutm32euref89 IS NOT NULL)
				OR (b2.xutm IS NOT NULL AND b2.yutm IS NOT NULL)
				OR b2.geom IS NOT NULL
		)
	;
	
	-- Qry_position_3 -- coordinates exist and elevation credible
	UPDATE mstjupiter.borehole_quality
	SET quality_position = 3
	WHERE quality_position > 1
		AND boreholeid IN 
			(
				SELECT DISTINCT b2.boreholeid
				FROM jupiter.borehole b2
				WHERE upper(b2.elevametho) IN ('P', 'N')
			)
	;
  	
	-- Qry_position_4 -- coordinates by gps
 	UPDATE mstjupiter.borehole_quality
	SET quality_position = 4
    WHERE quality_position > 1
   		AND boreholeid IN 
			(
				SELECT DISTINCT b2.boreholeid
				FROM jupiter.borehole b2
				WHERE upper(b2.locatmetho) IN ('G')	
			)
	;
   
	-- Qry_position_5 -- coordinates by gps and elevation credible    
   	UPDATE mstjupiter.borehole_quality
	SET quality_position = 5
    WHERE quality_position > 3
		AND boreholeid IN 
			(
				SELECT DISTINCT b2.boreholeid
				FROM jupiter.borehole b2
				WHERE upper(b2.elevametho) IN ('P', 'N')
			)
	;
			
    -- Qry_position_6 -- coordinates by DGPS, GPS-RTK or surveyor
   	UPDATE mstjupiter.borehole_quality
    SET quality_position = 6
    WHERE quality_position > 1
   		AND boreholeid IN 
			(
				SELECT DISTINCT b2.boreholeid
				FROM jupiter.borehole b2
				WHERE upper(b2.locatmetho) IN ('DG', 'GR', 'I')	
			)
	;

	-- Qry_lithodata_1 -- exist drilling/soil-sample report
    UPDATE mstjupiter.borehole_quality AS b
    SET quality_litho = 1
	WHERE b.boreholeid IN 
		(
			SELECT DISTINCT bd.boreholeid
			FROM jupiter.boredoc bd
			WHERE upper(bd.doctype) IN ('B', 'BG', 'J') 
		)
	;

	-- Qry_lithodata_2 -- rocksymbol not null or 
	-- only discribed as unknown/well-casing/no-data
    UPDATE mstjupiter.borehole_quality AS b
    SET quality_litho = 3
    WHERE b.boreholeid IN 
		(
			SELECT DISTINCT l.boreholeid
			FROM jupiter.lithsamp l 
			WHERE COALESCE(lower(l.rocksymbol),'x') NOT IN ('x', 'b', 'u')
		)
	;
   
   	-- Qry_lithodata_3 -- DAPCO boreholes
    UPDATE mstjupiter.borehole_quality AS b
    SET quality_litho = 2
	WHERE quality_litho > 1
		AND b.boreholeid IN 
			(
				SELECT DISTINCT b2.boreholeid
				FROM jupiter.borehole b2
				WHERE upper(b2.purpose) = 'H'
			)
	;
		
    -- Qry_lithodata_4 -- lithology is 2 chars
    UPDATE mstjupiter.borehole_quality as b
    SET quality_litho = 4
    WHERE quality_litho > 2
    	AND b.boreholeid IN 
			(
				SELECT DISTINCT l.boreholeid 
				FROM jupiter.lithsamp l 
				WHERE char_length(l.rocksymbol) = 2
			)
	;
   
	-- Qry_lithodata_5 -- defined drilldepth
	UPDATE mstjupiter.borehole_quality AS b
	SET quality_litho = 5
	WHERE quality_litho >= 4
		AND b.boreholeid IN 
			(
				SELECT DISTINCT b2.boreholeid
				FROM jupiter.borehole b2
				WHERE b2.drilldepth IS NOT NULL AND b2.drilldepth != 0
			)
	;
		
    -- Qry_lithodata_6 -- 1 lithsample per 2 meter drilldepth
	UPDATE mstjupiter.borehole_quality AS b
	SET quality_litho = 6
	FROM 
		(
			SELECT ls.boreholeid, b2.drilldepth/COUNT(ls.sampleno) AS meter_per_sample
			FROM jupiter.lithsamp ls
			LEFT JOIN jupiter.borehole b2 ON ls.boreholeid = b2.boreholeid
			WHERE b2.drilldepth IS NOT NULL OR b2.drilldepth != 0
			GROUP BY ls.boreholeid, b2.drilldepth
		) AS numofsamp
	WHERE quality_litho > 4
		AND b.boreholeid = numofsamp.boreholeid
		AND numofsamp.meter_per_sample <= 2;
	
    -- Qry_lithodata_7 -- Drillmethod is 
    UPDATE mstjupiter.borehole_quality AS b
    SET quality_litho = 7
    WHERE quality_litho > 5
    	AND b.boreholeid IN 
			(
				SELECT DISTINCT dm.boreholeid
				FROM jupiter.drilmeth AS dm
				WHERE upper(dm.METHOD) IN ('G', 'T', 'I', 'L', 'R')
			)
	;
	
    -- LITHO QUALITY SUM
    ALTER TABLE mstjupiter.borehole_quality 
   	ADD COLUMN quality_litho_position INTEGER DEFAULT 0,
   	ADD COLUMN quality_litho_position_descr text DEFAULT 'No lithology data or no geographical location';
   
  	UPDATE mstjupiter.borehole_quality AS b
    SET 
		quality_litho_position = 1,
		quality_litho_position_descr = '1) Lithology description is not determined by soil sample description and 2) coordinates are not made by gps.'
    WHERE quality_litho > 0 
   		AND quality_position < 4
   		AND quality_position > 1;
   
	UPDATE mstjupiter.borehole_quality AS b
    SET 
		quality_litho_position = 2,
		quality_litho_position_descr = '1) Lithology description is not determined by soil sample description and 2) coordinates are made by gps.'
	WHERE quality_litho > 0 
		AND quality_position > 3;
    
   	UPDATE mstjupiter.borehole_quality AS b
    SET 
		quality_litho_position = 3,
		quality_litho_position_descr = '1) Lithology description is determined by soil sample description, 2) coordinates are not made by gps and 3) borehole is demolished.'
    WHERE quality_litho > 3
   		AND quality_position < 4
   		AND quality_position > 1
   		AND is_demolished = 't';
   
   	UPDATE mstjupiter.borehole_quality AS b
    SET 
		quality_litho_position = 4,
		quality_litho_position_descr = '1) Lithology description is determined by soil sample description, 2) coordinates are not made by gps and 3) borehole is not demolished.'
    WHERE quality_litho > 3
   		AND quality_position < 4
   		AND quality_position > 1
   		AND is_demolished = 'f';
  
  	UPDATE mstjupiter.borehole_quality AS b
    SET 
		quality_litho_position = 5,
		quality_litho_position_descr = '1) Lithology description is determined by soil sample description and 2) coordinates are made by gps.'
    WHERE quality_litho > 3
    	AND quality_position > 3;
    
   	UPDATE mstjupiter.borehole_quality AS b
    SET 
		quality_litho_position = 6,
		quality_litho_position_descr = '1) Lithology description is determined by soil sample description, 2) coordinates are made by gps and 3) drillmethod is good or there are maximum of 2 m between lithology samples'
    WHERE quality_litho > 5
    	AND quality_position > 3;	
    
	GRANT SELECT ON ALL TABLES IN SCHEMA mstjupiter TO jupiterrole;
	
COMMIT;