BEGIN;
	
	-- create empty columns for dhm_jupiter data
	ALTER TABLE mstjupiter.borehole_quality
		ADD COLUMN z_dhm NUMERIC DEFAULT NULL,
		ADD COLUMN z_jup NUMERIC DEFAULT NULL,
		ADD COLUMN dist_z NUMERIC DEFAULT NULL,
		ADD COLUMN dist_xy NUMERIC DEFAULT NULL,
		ADD COLUMN dhm_datetime timestamp DEFAULT NULL,
		ADD COLUMN url TEXT DEFAULT NULL
	;

	-- insert dhm_jupiter data into borehole_quality table
	UPDATE mstjupiter.borehole_quality bq
		SET 
			z_dhm = dj.z_dhm,
			z_jup = dj.z_jup,
			dist_z = dj.dist_z,
			dist_xy = dj.dist_xy,
			dhm_datetime = dj.datetime::timestamp,
			url = concat('http://data.geus.dk/JupiterWWW/borerapport.jsp?borid=', dj.boreholeid)
	FROM dhm.dhm_jupiter dj
	WHERE bq.boreholeid = dj.boreholeid
	;
		
	-- Create index elevation difference
	CREATE INDEX borehole_quality_ele_idx
  		ON mstjupiter.borehole_quality
			USING BTREE (dist_z)
	;	

	GRANT SELECT ON ALL TABLES IN SCHEMA mstjupiter TO jupiterrole;

COMMIT;