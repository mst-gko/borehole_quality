BEGIN;
	--Make view from borehole quality and elevation-difference 
	DROP MATERIALIZED VIEW IF EXISTS mstjupiter.mstmvw_borehole_quality;
	DROP TABLE IF EXISTS mstjupiter.mstmvw_borehole_quality;

	CREATE TABLE mstjupiter.mstmvw_borehole_quality as (
		SELECT 
			ROW_NUMBER() over (ORDER BY bq.boreholeno) AS row_id, 
			bq.boreholeno, 
			bq.boreholeid, 
			bq.drilldepth_edit, 
			bq.quality_position, 
			bq.quality_litho, 
			bq.quality_litho_position,
			bq.quality_litho_position_descr,
			dj.z_dhm, 
			dj.z_jup, 
			dj.dist_z,
			dj.dist_xy,
			dj.datetime AS dhm_datetime,
			concat('http://data.geus.dk/JupiterWWW/borerapport.jsp?borid=',bq.boreholeid) as url,
			bq.geom 	
		FROM mstjupiter.borehole_quality bq
		LEFT JOIN dhm.dhm_jupiter dj USING (boreholeid)
		--LEFT JOIN jupiter.watlevel wl USING (boreholeid)
		-- WHERE edj.elevation_diff IS NOT NULL --testing purpose
	);

	-- Create index
	CREATE INDEX mstmvw_borehole_quality_idx1
  		ON mstjupiter.mstmvw_borehole_quality
			USING BTREE (boreholeid);
		
	-- Create index elevation
	CREATE INDEX mstmvw_borehole_quality_idx2
  		ON mstjupiter.mstmvw_borehole_quality
			USING BTREE (dist_xy);

	-- Create geometry index
	CREATE INDEX mstmvw_borehole_quality_geom_idx
  		ON mstjupiter.mstmvw_borehole_quality
			USING GIST (geom);

	GRANT SELECT ON ALL TABLES IN SCHEMA mstjupiter TO jupiterrole;

COMMIT;