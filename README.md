# Borehole quality

## Description
The lithological quality assessment of boreholes, 
is a SQL script that provides an arbitrary quality assessment 
of boreholes in the Jupiter database. 
SQL scripts have been developed for the following: 

- Postgresql database, 
where MST Groundwater runs on a national Postgresql database 
(quality_borehole_FDB.sql).

- Firebird database, where the SQL script is designed to load data into a 
geological model, eg Geoscene3d (quality_borehole_FDB.sql).

The sql file 'quality_borehole.sql' yields a 
quality category of lithology samples and the borehole position.

- [Flowchart](https://gitlab.com/mst-gko/borehole_quality/-/blob/master/lithology_quality_flowchart.png)
- [Description (danish)](https://gitlab.com/mst-gko/borehole_quality/-/blob/master/Beskrivelse_af_lithologisk_kvalitetsvurdering_af_boringer.docx)

![Flowchart](https://gitlab.com/mst-gko/borehole_quality/-/raw/master/lithology_quality_flowchart.png)

The quality assessment is built up by setting boundary intervals 
on existing fields in the database. No analyzes will be made 
in relation to external data such as Geus' "jordartskort", 
geophysics or the danish elevation model and the wells are categorized based 
only on descriptions of the individual wells in the Jupiter database.

## Output
### In general
The script provides a quality assessment of:
- <b> How certain is the borehole location in xyz.  </b> 

- <b> How good is the lithological sample description of the bore.  </b> 

- <b> A combined quality assessment of both drilling location 
and sample description. </b> 

The latter combined quality assessment can be described as follows:
- <b> Category 0: </b> 
Borehole has either a low data quality 
or lacks the location and should not be used

- <b> Category 1:  </b> 
Lithology description is inferior and placement is uncertain.	

- <b> Category 2: </b> 
Lithology description is inferior but placement is certain.	

- <b> Category 3: </b> 
Lithology description good, 
location uncertain and borehole is demolished.

- <b> Category 4: </b> 
Lithology description good and location uncertain, 
but borehole is not demolished.

- <b> Category 5: </b> 
Lithology description is good and location certain.

- <b> Category 6: </b>
Lithology description is very good and location certain.	

### Firebird
As the script is executed, 
new columns are generated in the existing 'borehole' table. 
This gives a blend of Geus' Jupiter data and the script derived data additions, 
but it makes it easier to load in eg geoscene3d.

- <b> quality_position: </b> 
The quality rating for the drilling position. 
The rating ranges from 0 to 6, with 6 being the best mark.

- <b> quality_litho: </b> 
The quality assessment for the drilling lithological description. 
The rating ranges from 0 to 7, with 7 being the best mark.

- <b> quality_litho_position: </b> 
A combined quality assessment for the drilling position 
and lithological description. 
The rating ranges from 0 to 6, with 6 being the best mark.

### Postgresql
When the SQL script is executed, a new table named 'borehole_quality' 
is generated in the database schema 'mstjupiter' 
which contains a separate geometry for the borehole in the table.

- <b> row_id: </b> 
An unique row id. 
Note that this is not guaranteed to yield the same number for each run.

- <b> boreholeno: </b>
The DGU number defined in the Jupiter database. 
Note that this is the only unique identifier 
for boreholes in the Jupiter database.

- <b> boreholeid: </b>
The actual unique identifier for boreholes in the Jupiter database. 

- <b> drilldepth_edit: </b> 
Drilldepth from Jupiter database if this is not empty, 
or the bottom of the lowest layer in the lithology descriptions.

- <b> is_demolished: </b> 
A boolean true (1) / false (0) 
regarding wether the borehole is demolished and can be found during field work.

- <b> geom: </b> 
The geometry of the borehole retrieved from the Jupiter database.

- <b> quality_position: </b> 
A quality-assessment from 0 to 6 for the position of the borehole, 
see description in .docx in git repo.

- <b> quality_litho: </b> 
A quality-assessment from 0 to 7 for the lithological samples of the borehole, 
see description in .docx in git repo.

- <b> quality_litho_position: </b> 
A combined quality-assessment from 0 to 6 for the lithological samples 
and the position of the borehole, see flowchart and description
(.png and .docx) in git repo.

- <b> elevation_jupiter: </b> 
The surface-elevation of the borehole from the jupiter database.

- <b> elevation_dhm: </b> 
The surface-elevation of the borehole 
retrieved from the danish elevation model (DHM).

- <b> elevation_diff: </b> 
The difference of the surface-elevation of the borehole 
retrieved from the danish elevation model (DHM) and the Jupiter database.

- <b> elevation_diff_desc: </b> 
A description of the comparison between the Jupiter 
elevation and the danish elevation model (DHM). 
Eg. borehole outside the danish coastline is not covered by the DHM 
and the comparison is not compiled. 

- <b> url: </b> 
The url of the borehole report of the borehole.

## Help
For help contact [Simon Makwarth](mailto:simak@mst.dk)

## Author and License
Borehole_quality is written by Anders Damsgaard and Simon Makwarth, and is 
licensed under the GNU Public License version 3 or later. See 
[LICENSE](LICENSE) for details.
