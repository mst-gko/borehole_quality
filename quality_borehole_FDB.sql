-- Drop columns if they exist
EXECUTE block as
BEGIN
	IF
		( 
			EXISTS
				(
					SELECT 1 FROM RDB$RELATION_FIELDS rf
					WHERE rf.RDB$RELATION_NAME = 'BOREHOLE' 
						AND rf.RDB$FIELD_NAME = 'QUALITY_LITHO'
				)
		) 
		THEN execute statement 'ALTER TABLE BOREHOLE DROP QUALITY_LITHO';
	IF
		( 
			EXISTS
				(
					SELECT 1 FROM RDB$RELATION_FIELDS rf
					WHERE rf.RDB$RELATION_NAME = 'BOREHOLE' 
						AND rf.RDB$FIELD_NAME = 'QUALITY_LITHO_POSITION'
				)
		) 	
		THEN execute statement 'ALTER TABLE BOREHOLE DROP QUALITY_LITHO_POSITION';
	IF
		( 
			EXISTS
				(
					SELECT 1 FROM RDB$RELATION_FIELDS rf
					WHERE rf.RDB$RELATION_NAME = 'BOREHOLE' 
						AND rf.RDB$FIELD_NAME = 'QUALITY_POSITION'
				)
		) 
		THEN execute statement 'ALTER TABLE BOREHOLE DROP QUALITY_POSITION';
END
;

-- Add columns and set to 0
ALTER TABLE BOREHOLE 
	ADD quality_position integer,
	ADD quality_litho integer,
	ADD quality_litho_position INTEGER;

UPDATE BOREHOLE AS b
	SET 
		quality_position = 0,
		quality_litho = 0,
		quality_litho_position = 0
	WHERE b.BOREHOLEID = b.BOREHOLEID;
	

-- Qry_position_1_Lokaliseringsskema
UPDATE BOREHOLE AS b
SET quality_position = 1
WHERE b.boreholeid IN 
	(
		SELECT DISTINCT bd.boreholeid
		FROM boredoc bd
		WHERE upper(bd.doctype) = 'L' 
			--AND (sd.comments NOT ILIKE '%skan%' AND sd.comments NOT ILIKE '%scan%')
	)
;

-- Qry_position_2 -- coordinates exist
UPDATE BOREHOLE AS b
SET quality_position = 2
WHERE boreholeid IN 
	(
		SELECT DISTINCT b2.boreholeid
		FROM borehole b2
		WHERE (b2.xutm32euref89 IS NOT NULL AND b2.yutm32euref89 IS NOT NULL)
			OR (b2.xutm IS NOT NULL AND b2.yutm IS NOT NULL)
	)
;

-- Qry_position_3 -- coordinates exist and elevation credible
UPDATE BOREHOLE AS b
SET quality_position = 3
WHERE quality_position > 1
	AND boreholeid IN 
		(
			SELECT DISTINCT b2.boreholeid
			FROM borehole b2
			WHERE upper(b2.elevametho) IN ('P', 'N')
		)
;

-- Qry_position_4 -- coordinates by gps
UPDATE BOREHOLE AS b
SET quality_position = 4
WHERE quality_position > 1
	AND boreholeid IN 
		(
			SELECT DISTINCT b2.boreholeid
			FROM borehole b2
			WHERE upper(b2.locatmetho) IN ('G')	
		)
;
   
-- Qry_position_5 -- coordinates by gps and elevation credible    
UPDATE BOREHOLE AS b
SET quality_position = 5
WHERE quality_position > 3
	AND boreholeid IN 
		(
			SELECT DISTINCT b2.boreholeid
			FROM borehole b2
			WHERE upper(b2.elevametho) IN ('P', 'N')
		)
;
		
-- Qry_position_6 -- coordinates by DGPS, GPS-RTK or surveyor
UPDATE BOREHOLE AS b
SET quality_position = 6
WHERE quality_position > 1
	AND boreholeid IN 
		(
			SELECT DISTINCT b2.boreholeid
			FROM borehole b2
			WHERE upper(b2.locatmetho) IN ('DG', 'GR', 'I')	
		)
;

-- Qry_lithodata_1 -- exist drilling/soil-sample report
UPDATE BOREHOLE AS b
SET quality_litho = 1
WHERE b.boreholeid IN 
	(
	    SELECT DISTINCT bd.boreholeid
		FROM boredoc bd
		WHERE upper(bd.doctype) IN ('B', 'BG', 'J') 
	)
;

-- Qry_lithodata_2 -- rocksymbol not null or 
-- only discribed as unknown/well-casing/no-data
UPDATE BOREHOLE AS b
SET quality_litho = 3
WHERE b.boreholeid IN 
	(
		SELECT DISTINCT l.boreholeid
		FROM lithsamp l 
		WHERE COALESCE(lower(l.rocksymbol),'x') NOT IN ('x', 'b', 'u')
    )
;
   
-- Qry_lithodata_3 -- DAPCO boreholes
UPDATE BOREHOLE AS b
SET quality_litho = 2
WHERE quality_litho > 1
	AND b.boreholeid IN 
		(
			SELECT DISTINCT b2.boreholeid
			FROM borehole b2
			WHERE upper(b2.purpose) = 'H'
		)
;
	
-- Qry_lithodata_4 -- lithology is 2 chars
UPDATE BOREHOLE AS b
SET quality_litho = 4
WHERE quality_litho > 2
	AND b.boreholeid IN 
		(
	   		SELECT DISTINCT l.boreholeid 
			FROM lithsamp l 
			WHERE char_length(l.rocksymbol) = 2
		)
;
   
	-- Qry_lithodata_5 -- defined drilldepth
UPDATE BOREHOLE AS b
SET quality_litho = 5
WHERE quality_litho > 3
	AND b.boreholeid IN 
		(
			SELECT DISTINCT b2.boreholeid
			FROM borehole b2
			WHERE b2.drilldepth IS NOT NULL AND b2.drilldepth != 0
		)
;
	
-- Qry_lithodata_6 -- 1 lithsample per 2 meter drilldepth
UPDATE BOREHOLE AS b
SET quality_litho = 6
WHERE quality_litho > 4
	AND b.boreholeid IN 
		(
			WITH tmp AS 
				(
					SELECT ls.boreholeid, b2.drilldepth/COUNT(ls.sampleno) AS meters_per_sample
					FROM lithsamp ls
					LEFT JOIN borehole b2 ON ls.boreholeid = b2.boreholeid
					WHERE b2.drilldepth IS NOT NULL OR b2.drilldepth != 0
					GROUP BY ls.boreholeid, b2.drilldepth
				)
			SELECT boreholeid FROM tmp
			WHERE tmp.meters_per_sample <= 2
		)
;

-- Qry_lithodata_7 -- Drillmethod is 
UPDATE BOREHOLE AS b
SET quality_litho = 7
WHERE quality_litho > 5
	AND b.boreholeid IN 
		(
			SELECT DISTINCT dm.boreholeid
			FROM drilmeth AS dm
			WHERE upper(dm.METHOD) IN ('G', 'T', 'I', 'L', 'R')
		)
;

-- LITHO QUALITY SUM  
UPDATE BOREHOLE AS b
SET quality_litho_position = 1
WHERE quality_litho > 0 
	AND quality_position < 4
	AND quality_position > 1;
   
UPDATE BOREHOLE AS b
SET quality_litho_position = 2
WHERE quality_litho > 0 
	AND quality_position > 3;
    
UPDATE BOREHOLE AS b
SET quality_litho_position = 3
WHERE quality_litho > 3
	AND quality_position < 4
	AND quality_position > 1
	AND 
		(
			upper(b.use) = 'S'
			--OR b.use IS NULL
		)
;
   
UPDATE BOREHOLE AS b
SET quality_litho_position = 4
WHERE quality_litho > 3
	AND quality_position < 4
	AND quality_position > 1
	AND COALESCE(upper(b.use), 'test') != 'S' 
;
  
UPDATE BOREHOLE AS b
SET quality_litho_position = 5
WHERE quality_litho > 3
	AND quality_position > 3;
    
UPDATE BOREHOLE AS b
SET quality_litho_position = 6
WHERE quality_litho > 5
	AND quality_position > 3;	

